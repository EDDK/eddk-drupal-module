# EDDK drupal module

This module fetches the daily fire hazard map from the Civil Protection website and extracts hazard level from an image based on the Hue value of the pixels representing the Area of interest.  

It uses the feeds and XPath modules to retrieve the image and the features module for generating the hazard map content type, feeds importer and views.

It was developed for and deployed to https://dasoprostasia.gr/defcon-map

## Contribution

If you would like to help have a look at the issues 