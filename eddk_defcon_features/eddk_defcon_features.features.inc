<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function eddk_defcon_features_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => 1);
  }
  elseif ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => 2);
  }
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function eddk_defcon_features_imagecache_default_presets() {
  $items = array(
    'defcon-map-display' => array(
      'presetname' => 'defcon-map-display',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '450',
            'height' => '',
            'upscale' => 0,
          ),
        ),
      ),
    ),
    'node-gallery-thumbnail' => array(
      'presetname' => 'node-gallery-thumbnail',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => 100,
            'height' => 100,
          ),
        ),
      ),
    ),
    'sidebar' => array(
      'presetname' => 'sidebar',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '195',
            'height' => '',
            'upscale' => 0,
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function eddk_defcon_features_node_info() {
  $items = array(
    'defcon_map' => array(
      'name' => t('Χάρτης Επικινδυνότητας'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function eddk_defcon_features_views_api() {
  return array(
    'api' => '3.0',
  );
}
