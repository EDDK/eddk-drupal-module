<?php

/**
 * Implementation of hook_feeds_tamper_default().
 */
function eddk_defcon_features_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'defcon_map_rss-description-find_replace_regex';
  $feeds_tamper->importer = 'defcon_map_rss';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/.*img src="(.*?)".*/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['defcon_map_rss-description-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'defcon_map_rss-description-html_entity_decode';
  $feeds_tamper->importer = 'defcon_map_rss';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'html_entity_decode';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'HTML entity decode';
  $export['defcon_map_rss-description-html_entity_decode'] = $feeds_tamper;

  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'defcon_map_rss-description-rewrite';
  $feeds_tamper->importer = 'defcon_map_rss';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => 'src="[description]"',
  );
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Rewrite';
  $export['defcon_map_rss-description-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'defcon_map_rss-description-strip_tags';
  $feeds_tamper->importer = 'defcon_map_rss';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '<img>',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Strip tags';
  $export['defcon_map_rss-description-strip_tags'] = $feeds_tamper;

  $feeds_tamper = new stdClass;
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'defcon_map_rss-description-trim';
  $feeds_tamper->importer = 'defcon_map_rss';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Trim';
  $export['defcon_map_rss-description-trim'] = $feeds_tamper;

  return $export;
}
