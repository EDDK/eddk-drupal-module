<?php
function eddk_drush_command() {
  $items = array();

  $items['update-defcon-maps'] = array(
    'description' => "Update DEFCON maps.",
    // 'arguments' => array(
    //   'filling' => 'The type of the sandwich (turkey, cheese, etc.)',
    // ),
    // 'options' => array(
    //   'spreads' => 'Comma delimited list of spreads (e.g. mayonnaise, mustard)',
    // ),
    // 'examples' => array(
    //   'drush mmas turkey --spreads=ketchup,mustard' => 'Make a terrible-tasting sandwich that is lacking in pickles.',
    // ),
    'aliases' => array('eddkudl'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL, // No bootstrap at all.
  );

  return $items;
}

function drush_eddk_update_defcon_maps(){
  $sql = db_query("SELECT nid, title FROM {node} n WHERE language = '' AND type = 'defcon_map'");
  while ($node = db_fetch_object($sql)) {
    echo "Updating node $node->nid ($node->title)\n";
    $node = node_load($node->nid);
    node_save($node);
  }
}
